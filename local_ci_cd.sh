#!/bin/bash

npm run lint
npm run audit
npm run test

.build-client.sh

scp -r ./dist/* sshuser@server:/var/www/html/
